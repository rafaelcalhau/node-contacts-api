const {
  clearUrlLastSlash,
  respondWith404NotFound,
} = require('./httpHelpers');

const routers = [
  require('./ping').pingRouter,
  require('./contacts').contactsRouter,
  require('./contactDetails').contactDetailsRouter,
];

module.exports = function(request, response) {
  const appRouter = {};

  routers.forEach(router => {
    router.forEach(({ endpoint, verb, handle }) => {
      appRouter[`${verb} ${endpoint}`] = handle
    });
  })

  const requestUrl = clearUrlLastSlash(request.url);
  const appRouterKeys = Object.keys(appRouter);
  const routeKey = `${request.method} ${requestUrl}`;

  const routeHandlerKey = appRouterKeys.find(key => {
    const requestUrlPath = requestUrl.split('/');
    const [verb, endpoint] = key.split(' ');
    const endpointPath = endpoint.split('/');
    const match = `${verb} ${requestUrl}`.match(new RegExp(key));

    return match
      && request.method === verb
      && endpointPath.length === requestUrlPath.length;
  });

  const routeHandler = appRouter[routeHandlerKey];

  if (typeof routeHandler === 'function') {
    routeHandler(request, response);
  } else {
    const match = requestUrl.match(new RegExp(appRouter[routeKey]));
    respondWith404NotFound(response, { match, routeKey, routeHandler, url: requestUrl });
  }
};

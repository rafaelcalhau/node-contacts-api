const url = require('url');

module.exports = {
  clearUrlLastSlash: (url) => {
    if (url[url.length - 1] === '/') {
      url = url.slice(0, -1);
    }

    return url.replace('/?', '');
  },

  urlPathOf: (request) => url.parse(request.url).pathname,
  urlQueryOf: (request) => url.parse(request.url, true).query,

  respondWith200OkText: (response, textBody) => {
    response.writeHead(200, {
      'Content-Type': 'text/plain',
    });
    response.end(textBody);
  },

  respondWith204NoContent: (response, textBody) => {
    response.writeHead(204);
    response.end();
  },

  respondWith200OkJson: (response, jsonBody) => {
    response.writeHead(200, {
      'Content-Type': 'application/json',
    });
    response.end(JSON.stringify(jsonBody));
  },

  respondWith400BadRequest: (response, jsonBody) => {
    response.writeHead(400);
    response.end(jsonBody ? JSON.stringify(jsonBody) : undefined);
  },

  respondWith404NotFound: (response, jsonBody) => {
    response.writeHead(404);
    response.end(jsonBody ? JSON.stringify(jsonBody) : undefined);
  },

  respondWith500InternalServerError: (response) => {
    response.writeHead(500);
    response.end();
  },
};

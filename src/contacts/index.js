const { fakeDatabase } = require("../database/fakeDatabase");
const {
  respondWith200OkJson,
  respondWith400BadRequest,
  urlQueryOf,
} = require("../httpHelpers");
const { routerHandleResult } = require("../routerHandleResult");

const contactsRouter = [
  {
    endpoint: '/contacts',
    verb: 'GET',
    handle: (request, response) => {
      const qs = urlQueryOf(request);
      const limit = qs.limit !== undefined ? Number(qs.limit) : undefined;

      let contacts = fakeDatabase.selectAllFromContacts();
      contacts.sort((a, b) => a.name > b.name ? 1 : -1);

      if (limit !== undefined && (isNaN(limit) || limit < 0 || !Number.isInteger(limit))) {
        respondWith400BadRequest(response);
        return routerHandleResult.HANDLED;
      }
      
      if (qs.phrase !== undefined && qs.phrase.length === 0) {
        respondWith400BadRequest(response);
        return routerHandleResult.HANDLED;
      }
      
      if (qs.phrase) contacts = contacts.filter(contact => contact.name.toLowerCase().includes(qs.phrase.toLowerCase()))
      if (qs.limit) contacts = contacts.slice(0, qs.limit);
      respondWith200OkJson(response, contacts);

      return routerHandleResult.HANDLED;
    }
  },
];

module.exports = {
  contactsRouter
};

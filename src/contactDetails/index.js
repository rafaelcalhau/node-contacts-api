const { fakeDatabase } = require("../database/fakeDatabase");
const {
  respondWith404NotFound,
  respondWith200OkJson,
  respondWith500InternalServerError,
  respondWith204NoContent,
} = require("../httpHelpers");
const { routerHandleResult } = require("../routerHandleResult");

const contactDetailsRouter = [
  {
    endpoint: '\/contacts\/([0-9a-z-]+)',
    verb: 'GET',
    handle: (request, response) => {
      const id = request.url.split('/')[2];

      try {
        const contacts = fakeDatabase.selectFromContactsById(id);
        if (contacts.length === 0) {
          respondWith404NotFound(response)
        } else {
          respondWith200OkJson(response, contacts[0]);
        }
      } catch (error) {
        console.log(`Error: ${error.message}`)
        respondWith500InternalServerError(response);
      }

      return routerHandleResult.HANDLED;
    }
  },
  {
    endpoint: '\/contacts\/([0-9a-z-]+)',
    verb: 'DELETE',
    handle: (request, response) => {
      const id = request.url.split('/')[2];
      try {
        const contacts = fakeDatabase.selectFromContactsById(id);
        if (contacts.length === 0) {
          respondWith404NotFound(response);
        } else {
          fakeDatabase.deleteContactsById(id);
          respondWith204NoContent(response);
        }
      } catch (error) {
        console.log(`Error: ${error.message}`)
        respondWith500InternalServerError(response);
      }

      return routerHandleResult.HANDLED;
    }
  },
];

module.exports = {
  contactDetailsRouter
};
const { respondWith200OkText } = require("../httpHelpers");
const { routerHandleResult } = require("../routerHandleResult");

const pingRouter = [
  {
    endpoint: '/ping',
    verb: 'GET',
    handle: (request, response) => {
      respondWith200OkText(response, 'pong');
      return routerHandleResult.HANDLED;
    }
  },
];

module.exports = {
  pingRouter
};
